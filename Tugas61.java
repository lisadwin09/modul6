import java.awt.Graphics;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JFrame;

public class Tugas61 extends JPanel {
	
	public  void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.setBackground(Color.WHITE);
		
		g.setColor(new Color(255, 0, 0));
		g.fillRect(15,  25,  100, 20);
		g.drawString("RED : " + g.getColor(), 130, 40);
		
		g.setColor(new Color(0.50f, 0.75f, 0.0f));
		g.fillRect(15,  50,  100, 20);
		g.drawString("GREEN : " + g.getColor(), 130, 65);
		
		g.setColor(Color.BLUE);
		g.fillRect(15,  75,  100, 20);
		g.drawString("BLUE : " + g.getColor(), 130, 90);
		
		g.setColor(Color.BLACK);
		g.fillRect(15,  100,  100, 20);
		g.drawString("BLACK : " + g.getColor(), 130, 115);
		
		g.setColor(Color.YELLOW);
		g.fillRect(15,  125,  100, 20);
		g.drawString("YELLOW : " + g.getColor(), 130, 140);
		
		g.setColor(Color.GRAY);
		g.fillRect(15,  150,  100, 20);
		g.drawString("GRAY : " + g.getColor(), 130, 165);
		
		g.setColor(Color.ORANGE);
		g.fillRect(15,  175,  100, 20);
		g.drawString("ORANGE : " + g.getColor(), 130, 190);
		
		g.setColor(Color.PINK);
		g.fillRect(15,  200,  100, 20);
		g.drawString("PINK : " + g.getColor(), 130, 215);
		
		g.setColor(Color.MAGENTA);
		g.fillRect(15,  225,  100, 20);
		g.drawString("MAGENTA : " + g.getColor(), 130, 240);
		
		g.setColor(Color.CYAN);
		g.fillRect(15,  250,  100, 20);
		g.drawString("CYAN : " + g.getColor(), 130, 265);		
	}
	
	public static void main (String args[]) {
		JFrame frame = new JFrame ("Using Colors");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Tugas61 colorJPanel = new Tugas61();
		frame.add(colorJPanel);
		frame.setSize(400, 400);
		frame.setVisible(true);
	}
}