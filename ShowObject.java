import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ShowObject extends JPanel {
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.setBackground(Color.WHITE);
		
		Color colorRect = Color.GREEN;
		Color colorOval = Color.BLUE;
		Color colorPolygon = Color.PINK;
		Color colorArc = Color.DARK_GRAY;
		Color colorText = Color.BLACK;
		
		//Draw Rectangle
		g.setColor(colorRect);
		g.fillRect(15, 25, 80, 30);
		g.setColor(colorText);
		g.drawString("This is Rectangle ", 15, 70);
		
		//Draw Oval
		g.setColor(colorOval);
		g.fillOval(15, 90, 90, 30);
		g.setColor(colorText);
		g.drawString("This is Oval ", 15, 140);
		
		//Draw Polygon
		g.setColor(colorPolygon);
		int[] xPoints = {55, 15, 35, 75, 95};
		int[] yPoints = {150, 180, 210, 210, 180};
		int nPoints = 5;
		g.fillPolygon(xPoints, yPoints, nPoints);
		g.setColor(colorText);
		g.drawString("This is Polygon ", 15, 230);
		
		//Draw Arc
		g.setColor(colorArc);
		g.fillArc(15, 250, 60, 60, 0, 270);
		g.setColor(colorText);
		g.drawString("This is Arc ", 15, 330);
	}
		
	public static void main(String[] args) {
		JFrame callFrame = new JFrame("Drawing Object at JFrame");
		callFrame.setSize(150, 380);
		callFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		callFrame.setContentPane(new ShowObject());
		callFrame.setVisible(true);
	}
}