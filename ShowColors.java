 import javax.swing.JFrame;
 import java.awt.Color;
 import javax.swing.JPanel;

public class ShowColors {
	public static void main (String args[]) {
		JFrame frame = new JFrame("using colors");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ColorJPanel colorJPanel = new ColorJPanel();
		
		frame.add(colorJPanel);
		frame.setSize(400,180);
		frame.setVisible(true);
	}
}