import javax.swing.*;
import java.awt.*;
import java.awt.geom.GeneralPath;

public class Stars extends JPanel {
	
	public void paintComponent(Graphics g) {
		int xPoints[] = {55, 67, 109, 73, 83, 55, 27, 37, 1, 43 };
		int yPoints[] = {0, 33, 36, 54, 96, 72, 96, 54, 36, 36 };
		Graphics2D g2D = (Graphics2D)g;
		GeneralPath star = new GeneralPath();
		star.moveTo(xPoints[0], yPoints[0]);
		
		for (int i = 0; i < xPoints.length; i++)
			star.lineTo(xPoints[i], yPoints[i]);
		star.closePath();
		g2D.translate(200, 200);
		
		for (int j = 0; j < 20; j++) {
			g2D.rotate(Math.PI / 10.0);
			g2D.setColor(new Color((int) (Math.random()*256), (int)(Math.random()*256), (int)(Math.random()*256)));
			g2D.fill(star);
		}
	}
		
	public static void main (String[] args) {
		JFrame frame = new JFrame();
		frame.setTitle("Drawing 2D Shapes");
		frame.setSize(400, 400);
		frame.add(new Stars());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
}