import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.geom.Line2D;
import java.awt.geom.Arc2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;

public class ShowObject2 extends JPanel {
	public double rectHeight = 30;	
	public double rectWidth = 95;
	private Stroke stroke;
	
	final static float dash1[] = {10.0f};
    final static BasicStroke dashed =
        new BasicStroke(1.0f,
                        BasicStroke.CAP_BUTT,
                        BasicStroke.JOIN_MITER,
                        10.0f, dash1, 0.0f);
	
    final static BasicStroke wideStroke = new BasicStroke(8);
    
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		
		this.setBackground(Color.WHITE);
		
		Color colorLine = Color.DARK_GRAY;
		Color colorRect = Color.BLUE;
		Color colorRoundRect = Color.GREEN;
		Color colorArc = Color.DARK_GRAY;
		Color colorText = Color.BLACK;
		
		//Draw Line2D
		g2.setColor(colorLine);
		g2.draw(new Line2D.Double(15.0, 15.0 + rectHeight-1, 15.0 + rectWidth, 15.0));
		g.setColor(colorText);
		g.drawString("This is Line ", 15, 70);
		
		//Draw Rectangle2D
		g2.setColor(colorRect);
		//g2.setStroke(stroke);
		g2.draw(new Rectangle.Double(15, 90, rectWidth, rectHeight));
		g.setColor(colorText);
		g.drawString("This is Rectangle ", 15, 150);
		
		//Draw RoundRectangle2D
		g2.setColor(colorRoundRect);
		g2.setStroke(dashed);
		g2.draw(new RoundRectangle2D.Double(15, 170, rectWidth, rectHeight, 10, 10));
		g.setColor(colorText);
		g.drawString("This is Round Rectangle ", 15, 230);
		
		//Draw Arc2D
		g2.setColor(colorArc);
		g2.setStroke(wideStroke);
		g2.draw(new Arc2D.Double(15, 255, rectWidth, rectHeight, 90, 135, Arc2D.OPEN));
		g.setColor(colorText);
		g.drawString("This is Open Arc ", 15, 310);

	}
		
	public static void main(String[] args) {
		JFrame callFrame = new JFrame("Drawing Object at JFrame");
		callFrame.setSize(180, 380);
		callFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		callFrame.setContentPane(new ShowObject2());
		callFrame.setVisible(true);
	}
}